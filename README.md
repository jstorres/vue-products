# Vue-Products
Basic setup with:

1. Vue
2. Vite
3. TypeScript
4. Vuex
5. Tailwind CSS
5. Auto 1. Import
6. PostCSS
7. VueRouter
8. Axios


### Install Project

```
npm install

```
or

```
yarn 

```

### Run Project

```
npm run dev

```
or

```
yarn dev

```
### About this Project

This is a test project, it implements the libraries mentioned above, the objective is to list a series of products with a filter and search and a shopping cart.

### Developer information

1. see me [Linkedin] (https://www.linkedin.com/in/juan-sebastian-torres/).
2. see me [Gitlab] (https://gitlab.com/jstorres).
3. see me [Github] (https://github.com/jstcode99).

#### About Me

SEBASTIAN TORRES


I love to develop tools that provide
quick and precise solutions,
technological tools are being
focusing on helping others solve
problems and help control any type
of processes, by making use of new
technologies that provide assurance
as well as learning which new
technological tools are being
developed and how they are being
developed by different companies.