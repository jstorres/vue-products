export {useCategoriesStore} from "@/composables/useCategoriesStore";
export {useProductsStore} from "@/composables/useProductsStore";
export {useAuthStore} from "@/composables/useAuthStore";
export {useCartStore} from "@/composables/useCartStore";


