import {useStore} from "vuex";
import {IState} from "@/store";
import {IAuth} from "@/interfaces/IUser";
export const useAuthStore = () => {
  const store = useStore<IState>()
  
  return {
    user: computed(() => store.state.user.user),
    isLoading: computed(() => store.state.user.isLoading),
    isAuth: computed(() => store.state.user.isAuth),
    authUser: (auth: IAuth) => {
      store.commit("user/setIsLoading", false);
      store.dispatch('user/authUser', auth)
    },
  }
}