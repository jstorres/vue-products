import {useStore} from "vuex";
import {IState} from "@/store";
import {onMounted} from "vue";
import {IProduct} from "@/interfaces/IProduct";

export const useCartStore = () => {
  const store = useStore<IState>()

  onMounted(() => {
    if (!store.getters['cart/getIDt']) {
      store.dispatch('cart/saveCart').then(r => r)
    }
  })

  const callOperation = (
    operation: 'addProduct' | 'decreaseProduct' | 'deleteProduct',
    product: IProduct | undefined,
  ) => {
    store.commit(`cart/${operation}`, product)
    store.commit('cart/calcValues')
  }

  return {
    isLoading: computed(() => store.state.cart.isLoading),
    products: computed(() => store.state.cart.products),
    subtotal: computed(() => store.state.cart.subtotal),
    taxes: computed(() => store.state.cart.taxes),
    total: computed(() => store.state.cart.total),
    addProduct: (product: IProduct | undefined) => callOperation("addProduct", product),
    decreaseProduct: (product: IProduct | undefined) => callOperation("decreaseProduct", product),
    deleteProduct: (product: IProduct | undefined) => callOperation("deleteProduct", product),
  }
}