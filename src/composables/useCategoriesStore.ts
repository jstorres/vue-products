import {useStore} from "vuex";
import {IState} from "@/store";
import {onMounted} from "vue";

export const useCategoriesStore = () => {
  const store = useStore<IState>()

  onMounted(() => {
    if (!store.getters['categories/getIsLoading']) {
      store.dispatch('categories/getCategories').then(r => r)
    }
  })

  return {
    categories: computed(() => store.state.categories.categories),
  }
}