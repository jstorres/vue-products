import {useStore} from "vuex";
import {IState} from "@/store";
import {onMounted} from "vue";

export const useProductsStore = () => {
  const store = useStore<IState>()

  onMounted(() => {
    if (store.getters['products/getProducts'].length === 0) {
        store.dispatch('products/fetchAllProducts').then(r => r)
    }
  })

  const getProducts = () => {
    store.commit("products/setIsLoading", false);
    if (store.getters['products/getCategory'] !== '') {
      store.dispatch('products/fetchProductByCategory')
    } else {
      store.dispatch('products/fetchAllProducts')
    }
  }
  const setCategory = (category: string) => {
    store.commit('products/setCategory', category)
    getProducts()
  }

  const setSort = (sort: string) => {
    store.commit('products/setSort', sort)
    getProducts()
  }

  const setSearch = (search: string) => {
    store.commit('products/setSearch', search)
    getProducts()
  }

  return {
    isLoading: computed(() => store.state.products.isLoading),
    products: computed(() => store.state.products.products),
    product: computed(() => store.state.products.product),
    fetchProduct: (id: number) => {
      store.commit("products/setIsLoading", false);
      store.dispatch('products/fetchProduct', id)
    },
    sort: computed(() => store.state.products.sort),
    category: computed(() => store.state.products.catergory),
    search: computed(() => store.state.products.search),
    setSort: setSort,
    setCategory: setCategory,
    setSearch: setSearch
  }
}