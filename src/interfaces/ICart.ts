import {IProduct} from "@/interfaces/IProduct";

export interface ICart  {
  id:number,
  userId: number,
  date: string,
  products: IProduct[]
}