export interface IProduct {
  id:number,
  title:string,
  price:number,
  category: string,
  description:string,
  image:string
}

export interface IProductCart extends IProduct {
  quantity: number
}

export interface IProductCartAPI {
  productId: number,
  quantity: number
}


export type TSortBy = 'desc' | 'asc'