export interface IGeolocation {
  lat: string,
  long:string
}
export interface IAddress {
  city: string,
  street: string,
  number: number,
  zipcode: string,
  geolocation: IGeolocation
}

export interface INameUser {
  firstname: string,
  lastname: string
}

export interface IAuth {
  username: string,
  password: string,
}

export interface IUser  {
  id: number,
  email: string,
  username: string,
  password: string,
  name: INameUser,
  address: IAddress,
  phone: string
}

