import * as vR from "vue-router";
import HomePage from "@/pages/HomePage.vue";
import PageNotFoundPage from "@/pages/NotFoundPage.vue";
import ProductPage from "@/pages/ProductPage.vue";
import ShoppingCartPage from "@/pages/ShoppingCartPage.vue";

const _routes:Array<vR.RouteRecordRaw> = [
  {
      path:"/",
      component: HomePage,
      name:"home"
  },
  {
      path: '/:pathMatch(.*)*',
      name: 'page.no.found',
      component: PageNotFoundPage
  },
  {
      path:"/product/:id",
      component:ProductPage,
      name:"single.product"
  },
  {
    path:"/shopping-cart",
    component:ShoppingCartPage,
    name:"shopping.cart"
  },
];

const router = vR.createRouter({
  history: vR.createWebHistory(), //WebHash history will add hash before every route
  routes: _routes, //Send the routes here.
});

export default router;
