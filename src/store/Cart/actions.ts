import { ActionTree } from 'vuex';
import { IState } from '@/store';
import axios from "axios";
import {ICart} from "./state";
import dayjs from "dayjs";
import {IProductCart, IProductCartAPI} from "@/interfaces/IProduct";

const actions: ActionTree<ICart, IState> = {
  async saveCart({rootState, state}) {
    try {
      await axios.post("https://fakestoreapi.com/carts", {
        userId: rootState.user?.id,
        date: dayjs().format('Y-M-d'),
        products: state.products.map((product: IProductCart) => {
          return { productId: product.id, quantity: product.quantity}
        })
      });
    } catch (error) {
      alert(error);
      console.log(error);
    }
  },
  async updateCart({rootState, state}) {
    try {
      await axios.put(`https://fakestoreapi.com/carts/${state.id}`, {
        userId: rootState.user?.id,
        date: dayjs().format('Y-M-d'),
        products: state.products.map((product: IProductCart) => {
          return { productId: product.id, quantity: product.quantity}
        })
      });
    } catch (error) {
      alert(error);
      console.log(error);
    }
  },
  async fetchCart({commit, state}) {
    try {
      const data = await axios.get(`https://fakestoreapi.com/carts/${state.id}`)
        .then(() => {
           const productsWithData = data.data.products.map(async (product: IProductCartAPI) => {
            const data = await axios.get(`https://fakestoreapi.com/products/${product.productId}`);
            return {...data.data, quantity: product.quantity }
          })
          commit("setProducts", productsWithData);
      });
      if (data.data) {
        commit("setIsLoading", true);
      }
    } catch (error) {
      alert(error);
      console.log(error);
    }
  },
}

export default actions;