import { GetterTree } from 'vuex';
import {IState} from "@/store";
import {ICart} from "./state";
import {IProductCart} from "@/interfaces/IProduct";

const getters: GetterTree<ICart, IState> = {
  getID(state: ICart): number | null {
    return state.id
  },
  getProducts(state: ICart): IProductCart[] {
    return state.products
  },
  getSubTotal(state: ICart): number {
    return state.subtotal
  },
  getTaxes(state: ICart): number {
    return state.taxes
  },
  getTotal(state: ICart): number {
    return state.total
  },
  getIsLoading(state: ICart): boolean {
    return state.isLoading
  }
}
export default getters;