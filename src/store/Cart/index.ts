import { Module } from 'vuex';
import { IState } from '@/store';

import state, {ICart} from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const cartModule: Module<ICart, IState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
}


export default cartModule;