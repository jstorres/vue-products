import { MutationTree } from 'vuex';
import {ICart} from "./state";
import {IProduct, IProductCart} from "@/interfaces/IProduct";

const mutation: MutationTree<ICart> = {
  addProduct(state: ICart, product: IProduct) {
    const index = state.products.findIndex(x => x.id === product.id)
    if (index !== -1) {
      state.products[index].quantity++
      state.products = [
        ...state.products
      ]
    } else {
      state.products.push({...product, quantity: 1})
    }
  },
  decreaseProduct(state: ICart, product: IProduct) {
    const index = state.products.findIndex(x => x.id === product.id)
    if (index !== -1) {
      state.products[index].quantity--
      state.products = [
        ...state.products
      ]
    }
  },
  deleteProduct(state: ICart, product: IProduct) {
    const filter = state.products.filter(x => x.id !== product.id)
    state.products = [...filter];
  },
  calcValues(state: ICart) {
    state.subtotal = state.products.reduce((acc, product) =>  { return acc + product.price * product.quantity; }, 0);
    state.taxes = 0.19 * state.subtotal;
    state.total = state.taxes + state.subtotal;
  },
  setIsLoading(state: ICart) {
    state.isLoading = true
  },
  setProducts(state: ICart, products: IProductCart[]) {
    state.products = products
  }
}
export default mutation;