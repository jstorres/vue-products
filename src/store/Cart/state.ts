import {IProductCart} from "@/interfaces/IProduct";

export interface ICart {
  isLoading: boolean
  id: number | null
  products: IProductCart[],
  subtotal: number,
  taxes: number
  total: number,
}
const state = (): ICart =>  {
  return {
    id: null,
    products: [],
    isLoading: false,
    subtotal: 0,
    taxes: 0,
    total: 0,
  }
}

export default state;