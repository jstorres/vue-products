import { ActionTree } from 'vuex';
import { IState } from '@/store';
import axios from "axios";
import {ICategoriesState} from "./state";

const actions: ActionTree<ICategoriesState, IState> = {
  async getCategories({commit}) {
    try {
      const data = await axios.get("https://fakestoreapi.com/products/categories");
      if (data.data) {
        commit("setIsLoading");
        commit("setCategories", data.data);
      }
    } catch (error) {
      alert(error);
      console.log(error);
    }
  }
}

export default actions;