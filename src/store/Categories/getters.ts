import { GetterTree } from 'vuex';
import {IState} from "@/store";
import {ICategoriesState} from "./state";

const getters: GetterTree<ICategoriesState, IState> = {
  getCategories(state: ICategoriesState): string[] {
    return state.categories
  },
  getIsLoading(state: ICategoriesState): boolean {
    return state.isLoading
  }
}
export default getters;