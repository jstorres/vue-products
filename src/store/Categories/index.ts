import { Module } from 'vuex';
import { IState } from '@/store';

import state, {ICategoriesState} from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const categoriesModule: Module<ICategoriesState, IState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
}


export default categoriesModule;