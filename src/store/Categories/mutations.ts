import { MutationTree } from 'vuex';
import {ICategoriesState} from "./state";


const mutation: MutationTree<ICategoriesState> = {
  setCategories(state: ICategoriesState, categories: string[]) {
    state.categories = categories
  },
  setIsLoading(state: ICategoriesState) {
    state.isLoading = true
  }
}
export default mutation;