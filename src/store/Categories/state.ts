export interface ICategoriesState {
  isLoading: boolean
  categories: string[]
}
const state = (): ICategoriesState =>  {
  return {
    isLoading: false,
    categories: []
  }
}

export default state;