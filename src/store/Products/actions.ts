import { ActionTree } from 'vuex';
import { IState } from '@/store';
import axios from "axios";
import {IProductsState} from "./state";
import {IProduct} from "@/interfaces/IProduct";

const filterBySearch = (products: IProduct[], search: string): IProduct[] => {
  if (search.length > 0) {
    return  products.filter((product) => product.title.toLowerCase().indexOf(search.toLowerCase()) > -1)
  }
  return products
}

const actions: ActionTree<IProductsState, IState> = {
  async fetchAllProducts({commit, state}) {
    try {
      const data = await axios.get(`https://fakestoreapi.com/products?sort=${state.sort}`);
      if (data.data) {
        commit("setIsLoading", true);
        commit("setProducts", filterBySearch(data.data, state.search));
      }
    } catch (error) {
      alert(error);
      console.log(error);
    }
  },
  async fetchProduct({commit}, id: number) {
    try {
      const data = await axios.get(`https://fakestoreapi.com/products/${id}`);
      if (data.data) {
        commit("setIsLoading", true);
        commit("setProduct", data.data);
      }
    } catch (error) {
      alert(error);
      console.log(error);
    }
  },
  async fetchProductByCategory({commit, state}) {
    try {
      const data = await axios.get(`https://fakestoreapi.com/products/category/${state.category}?sort=${state.sort}`);
      if (data.data) {
        commit("setIsLoading", true);
        commit("setProducts", data.data);
        commit("setProducts", filterBySearch(data.data, state.search));
      }
    } catch (error) {
      alert(error);
      console.log(error);
    }
  },
}

export default actions;