import { GetterTree } from 'vuex';
import {IState} from "@/store";
import {IProductsState} from "./state";
import {IProduct} from "@/interfaces/IProduct";


const getters: GetterTree<IProductsState, IState> = {
  getProducts(state: IProductsState): IProduct[] {
    return state.products
  },
  getIsLoading(state: IProductsState): boolean {
    return state.isLoading
  },
  getCategory(state: IProductsState): string {
    return state.category
  },
}
export default getters;