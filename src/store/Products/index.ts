import { Module } from 'vuex';
import { IState } from '@/store';

import state, {IProductsState} from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const productModule: Module<IProductsState, IState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
}


export default productModule;