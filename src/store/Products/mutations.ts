import { MutationTree } from 'vuex';
import {IProductsState} from "./state";
import {IProduct, TSortBy} from "@/interfaces/IProduct";


const mutation: MutationTree<IProductsState> = {
  setProducts(state: IProductsState, products: IProduct[]) {
    state.products = products
  },
  setProduct(state: IProductsState, product: IProduct) {
    state.product = product
  },
  setIsLoading(state: IProductsState, isLoading) {
    state.isLoading = isLoading
  },
  setSort(state: IProductsState, sort: TSortBy) {
    state.sort = sort
  },
  setSearch(state: IProductsState, search: string) {
    state.search = search
  },
  setCategory(state: IProductsState, category: string) {
    state.category = category
  }
}
export default mutation;