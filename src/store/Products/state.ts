import {IProduct, TSortBy} from "@/interfaces/IProduct";

export interface IProductsState {
  isLoading: boolean
  products: IProduct[]
  product: IProduct | null,
  sort: TSortBy
  search: string
  category: string
}
const state = (): IProductsState =>  {
  return {
    isLoading: false,
    products: [],
    product: null,
    sort: 'desc',
    category: '',
    search: ''
  }
}

export default state;