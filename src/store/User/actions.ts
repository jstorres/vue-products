import { ActionTree } from 'vuex';
import { IState } from '@/store';
import axios from "axios";
import {IUserState} from "./state";
import {IAuth} from "@/interfaces/IUser";

const actions: ActionTree<IUserState, IState> = {
  async getUser({commit}, id: number) {
    try {
      const data = await axios.get(`https://fakestoreapi.com/users/${id}`);
      if (data.data) {
        commit("setIsLoading");
        commit("setUser", data.data);
      }
    } catch (error) {
      alert(error);
      console.log(error);
    }
  },

  async authUser({commit, dispatch}, params: IAuth) {
    try {
      const data = await axios.post(`https://fakestoreapi.com/auth/login`, params);
      if (data.data) {
        commit("setIsLoading");
        commit("setIsAuth");
        commit("setToken", data.data);
        // no tengo como saber que id es el usuario authenticado por ello el 1 la api solo devuelve un token
        dispatch('getUser', 1)
      }
    } catch (error) {
      alert(error);
      console.log(error);
    }
  }
}

export default actions;