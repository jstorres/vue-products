import { GetterTree } from 'vuex';
import {IState} from "@/store";
import {IUserState} from "./state";
import {IUser} from "@/interfaces/IUser";

const getters: GetterTree<IUserState, IState> = {
  getUser(state: IUserState): IUser | null{
    return state.user
  },
  getIsAuth(state: IUserState): boolean {
    return state.isAuth
  },
  getIsLoading(state: IUserState): boolean {
    return state.isLoading
  },
}
export default getters;