import { Module } from 'vuex';
import { IState } from '@/store';

import state, {IUserState} from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const userModule: Module<IUserState, IState> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
}


export default userModule;