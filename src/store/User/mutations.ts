import { MutationTree } from 'vuex';
import {IUserState} from "./state";
import {IUser} from "@/interfaces/IUser";

const mutation: MutationTree<IUserState> = {
  setUser(state: IUserState, user: IUser) {
    state.user = user
  },
  setIsAuth(state: IUserState) {
    state.isAuth = true
  },
  setToken(state: IUserState, token: string) {
    state.token = token
  },
  setIsLoading(state: IUserState, isLoading) {
    state.isLoading = isLoading
  },
}
export default mutation;