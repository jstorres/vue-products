import {IUser} from "@/interfaces/IUser";

export interface IUserState {
  isAuth: boolean
  user: IUser | null
  token: string | null,
  isLoading: boolean

}
const state = (): IUserState =>  {
  return {
    isAuth: false,
    user: null,
    token: null,
    isLoading: false
  }
}

export default state;