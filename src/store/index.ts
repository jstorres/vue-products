import { createStore } from 'vuex';
import userModule from '@/store/User'
import categoriesModule from "@/store/Categories";
import productsModule from "@/store/Products";
import {IProduct} from "@/interfaces/IProduct";
import {IUser} from "@/interfaces/IUser";
import cartModule from "@/store/Cart";

export interface IState {
  user: IUser,
  products: IProduct[]
  categories: string[]
}


export default createStore<IState>({
  modules: {
    user: userModule,
    categories: categoriesModule,
    products: productsModule,
    cart: cartModule
  }
})
