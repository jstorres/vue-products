/** @type {import('tailwindcss').Config} */
module.exports = {
  //Specify the file formats where tailwind shoudl work
  content: ["./src/**/*.{html,js,jsx,tsx,vue}"],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
  ],
}
